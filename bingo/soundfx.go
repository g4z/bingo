package bingo

import (
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"github.com/gobuffalo/packr"
)

var (
	Wavbox packr.Box
	Ack    *Sample
)

type Sample struct {
	File   *os.File
	Format beep.Format
	Stream beep.StreamSeekCloser
}

func NewSample(filepath string) (*Sample, error) {
	s := &Sample{}
	file, err := os.Open(filepath)
	if err != nil {
		return s, err
	}
	s.File = file
	stream, format, err := wav.Decode(s.File)
	if err != nil {
		return s, err
	}
	s.Format = format
	s.Stream = stream
	return s, nil
}

func init() {

	// Create a new packr box for the audio files
	Wavbox = packr.NewBox("../wav")

	// Extract wav to disk because
	// we need an os.File handle :/
	data := Wavbox.Bytes("ok.wav")
	filepath, err := exportWavToDisk(data)
	if err != nil {
		log.Fatal(err)
	}

	// Load ACK wavfile sample
	ack, err := NewSample(filepath)
	if err != nil {
		log.Fatal(err)
	}

	Ack = ack

	// Configure output
	speaker.Init(
		Ack.Format.SampleRate,
		Ack.Format.SampleRate.N(time.Second/10))
}

func exportWavToDisk(data []byte) (string, error) {

	// Create a diskfile
	tmpfile, err := ioutil.TempFile("/tmp", "okbingowav")
	if err != nil {
		return "", err
	}

	defer tmpfile.Close()

	// Write the bytes
	if _, err := tmpfile.Write(data); err != nil {
		return "", err
	}

	return tmpfile.Name(), nil
}

func (s *Sample) Play() {

	// Seek to start of file
	s.Stream.Seek(0)

	// Play it
	speaker.Play(s.Stream)
}
