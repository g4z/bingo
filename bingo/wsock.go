package bingo

import (
	"log"
	"net/http"
	"time"

	"golang.org/x/net/websocket"
)

var totalPlayers int

type Packet struct {
	CurrentNumber   int   `json:"current_number"`
	PreviousNumbers []int `json:"previous_numbers"`
	TotalPlayers    int   `json:"total_players"`
}

func init() {
	totalPlayers = 0
}

func RegisterWS() {
	http.Handle("/ws", websocket.Handler(func(conn *websocket.Conn) {

		totalPlayers++

		// Track changing of number
		var prevCurrentNumber = 0
		for {

			// If the current number has changed...
			if State.GetCurrentNumber() > 0 &&
				prevCurrentNumber != State.GetCurrentNumber() {

				packet := &Packet{
					CurrentNumber:   State.GetCurrentNumber(),
					PreviousNumbers: State.GetPreviousNumbers(),
					TotalPlayers:    totalPlayers,
				}

				if err := websocket.JSON.Send(conn, packet); err != nil {
					log.Printf("[WARN] Failed write to websocket: %s\n", err)
					totalPlayers--
					break
				}
				prevCurrentNumber = State.GetCurrentNumber()
			}
			time.Sleep(1 * time.Second)
		}
	}))
}
