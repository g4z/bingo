package bingo

import (
	"log"
	"net/http"
	"sync"

	"github.com/gobuffalo/packr"
	config "github.com/spf13/viper"
)

func init() {
	log.Println("Loading HTTP module...")
	log.Printf("  http.public_dir => %s\n", config.GetString("http.public_dir"))
	log.Printf("  http.listen_addr => %s\n", config.GetString("http.listen_addr"))
}

func ListenHTTP(wg *sync.WaitGroup) {

	defer wg.Done()

	// Create a new packr box for the static content
	docroot := packr.NewBox("../" + config.GetString("http.public_dir"))

	// Serve static content from packr box
	http.Handle("/", http.FileServer(docroot))

	// Register WebSocket routes
	RegisterWS()

	// Register API routes
	RegisterAPI()

	// Startup

	err := http.ListenAndServe(
		config.GetString("http.listen_addr"),
		nil,
	)

	if err != nil {
		log.Fatal(err)
	}
}
