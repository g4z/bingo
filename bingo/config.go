package bingo

import (
	"fmt"

	"github.com/spf13/viper"
)

func init() {

	viper.SetConfigName("config")

	viper.AddConfigPath(".")
	// viper.AddConfigPath("$HOME/.appname")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}
