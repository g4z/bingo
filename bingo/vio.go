package bingo

import (
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"
	"unsafe"

	config "github.com/spf13/viper"
	"github.com/xlab/closer"
	"github.com/xlab/pocketsphinx-go/sphinx"
	"github.com/xlab/portaudio-go/portaudio"
)

const (
	samplesPerChannel = 512
	sampleRate        = 16000
	channels          = 1
	sampleFormat      = portaudio.PaInt16
)

func init() {
	log.Println("Loading VOICE module...")
	log.Printf("  voice.lang => %s\n", config.GetString("voice.lang"))
	log.Printf("  voice.speak_binary => %s\n", config.GetString("voice.speak_binary"))
	log.Printf("  voice.speak_voice => %s\n", config.GetString("voice.speak_voice"))
	log.Printf("  voice.speak_volume = %d", config.GetInt("voice.speak_volume"))
	log.Printf("  voice.speak_wordgap = %d", config.GetInt("voice.speak_wordgap"))
	log.Printf("  voice.speak_pitch = %d", config.GetInt("voice.speak_pitch"))
	log.Printf("  voice.speak_speed = %d", config.GetInt("voice.speak_speed"))
}

func Speak(buf string) {

	// go func(c string) {

	binary := config.GetString("voice.speak_binary")
	voice := config.GetString("voice.speak_voice")
	volume := fmt.Sprintf("%d", config.GetInt("voice.speak_volume"))
	wordgap := fmt.Sprintf("%d", config.GetInt("voice.speak_wordgap"))
	pitch := fmt.Sprintf("%d", config.GetInt("voice.speak_pitch"))
	speed := fmt.Sprintf("%d", config.GetInt("voice.speak_speed"))

	// println(binary, "-v", voice, "-a", volume,
	// 	"-g", wordgap, "-p", pitch, "-s", speed, buf)

	cmd := exec.Command(
		binary,
		"-v",
		voice,
		"-a",
		volume,
		"-g",
		wordgap,
		"-p",
		pitch,
		"-s",
		speed,
		buf)

	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}

	// }(b)
}

func ListenMic(wg *sync.WaitGroup) {

	defer closer.Close()
	defer wg.Done()

	closer.Bind(func() {
		log.Println("Bye!")
	})

	if err := portaudio.Initialize(); paError(err) {
		log.Fatalln("[ERR] PortAudio init error:", paErrorText(err))
	}

	closer.Bind(func() {
		if err := portaudio.Terminate(); paError(err) {
			log.Println("[ERR] PortAudio terminate error:", paErrorText(err))
		}
	})

	// Init CMUSphinx
	cfg := sphinx.NewConfig(
		sphinx.RawLogDirOption("/tmp/"),
		sphinx.LogFileOption("/tmp/sphinx.log"),
		sphinx.SampleRateOption(sampleRate),
	)

	if strings.ToLower(config.GetString("voice.lang")) == "it-it" {
		sphinx.HMMDirOption("/usr/local/share/pocketsphinx/model/it-IT/acoustic-model")(cfg)
		sphinx.LMFileOption("/usr/local/share/pocketsphinx/model/it-IT/language-model.lm.bin")(cfg)
		// sphinx.DictFileOption("/usr/local/share/pocketsphinx/model/it-IT/pronounciation-dictionary.dict")(cfg)
		sphinx.DictFileOption("dict/it.custom.dict")(cfg)
	} else {
		sphinx.HMMDirOption("/usr/local/share/pocketsphinx/model/en-us/en-us")(cfg)
		sphinx.LMFileOption("/usr/local/share/pocketsphinx/model/en-us/en-us.lm.bin")(cfg)
		//sphinx.LMFileOption("/usr/local/share/pocketsphinx/model/en-us/en-us-phone.lm.bin")(cfg)
		// sphinx.DictFileOption("/usr/local/share/pocketsphinx/model/en-us/cmudict-en-us.dict")(cfg)
		sphinx.DictFileOption("dict/en.custom.dict")(cfg)
	}

	dec, err := sphinx.NewDecoder(cfg)
	if err != nil {
		closer.Fatalln(err)
	}

	closer.Bind(func() {
		dec.Destroy()
	})

	l := &listener{
		dec: dec,
	}

	var stream *portaudio.Stream

	if err := portaudio.OpenDefaultStream(&stream, channels, 0, sampleFormat, sampleRate,
		samplesPerChannel, l.paCallback, nil); paError(err) {
		log.Fatalln("[ERR] PortAudio error:", paErrorText(err))
	}

	closer.Bind(func() {
		if err := portaudio.CloseStream(stream); paError(err) {
			log.Println("[WARN] PortAudio error:", paErrorText(err))
		}
	})

	if err := portaudio.StartStream(stream); paError(err) {
		log.Fatalln("[ERR] PortAudio error:", paErrorText(err))
	}

	closer.Bind(func() {
		if err := portaudio.StopStream(stream); paError(err) {
			log.Fatalln("[WARN] PortAudio error:", paErrorText(err))
		}
	})

	if !dec.StartUtt() {
		closer.Fatalln("[ERR] Sphinx failed to start utterance")
	}

	closer.Hold()
}

type listener struct {
	inSpeech   bool
	uttStarted bool
	dec        *sphinx.Decoder
}

func (l *listener) detectInput() {
	// Detect input and return if none
	if hyp, _ := l.dec.Hypothesis(); len(hyp) > 0 {
		log.Printf("DETECTED(%s)", hyp)
		ProcessInput(hyp)
	}
}

// paCallback: for simplicity reasons we process raw audio with sphinx in the this stream callback,
// never do that for any serious applications, use a buffered channel instead.
func (l *listener) paCallback(input unsafe.Pointer, _ unsafe.Pointer, sampleCount uint,
	_ *portaudio.StreamCallbackTimeInfo, _ portaudio.StreamCallbackFlags, _ unsafe.Pointer) int32 {

	const (
		statusContinue = int32(portaudio.PaContinue)
		statusAbort    = int32(portaudio.PaAbort)
	)

	in := (*(*[1 << 24]int16)(input))[:int(sampleCount)*channels]

	// ProcessRaw with disabled search because callback needs to be realtime
	_, ok := l.dec.ProcessRaw(in, true, false)

	// log.Printf("processed: %d frames, ok: %v", frames, ok)

	if !ok {
		return statusAbort
	}

	if l.dec.IsInSpeech() {

		l.inSpeech = true

		if !l.uttStarted {
			l.uttStarted = true
			// log.Println("Listening..")
		}

	} else if l.uttStarted {

		// speech -> silence transition, time to start new utterance
		l.dec.EndUtt()
		l.uttStarted = false

		l.detectInput() // report results

		if !l.dec.StartUtt() {
			closer.Fatalln("[ERR] Sphinx failed to start utterance")
		}
	}

	return statusContinue
}

func paError(err portaudio.Error) bool {
	return portaudio.ErrorCode(err) != portaudio.PaNoError
}

func paErrorText(err portaudio.Error) string {
	return portaudio.GetErrorText(err)
}
