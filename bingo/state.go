package bingo

import (
	"errors"
	"fmt"
)

type state struct {
	currentNumber   int
	previousNumbers []int
}

var State *state

func init() {
	State = &state{0, []int{}}
}

func (s *state) AddNumber(number int) error {

	// Check number hasn't previously been added
	for _, v := range s.previousNumbers {
		if number == v {
			return errors.New("Number already used")
		}
	}

	// Save currentNumber
	if s.currentNumber > 0 {
		s.previousNumbers = append(
			s.previousNumbers,
			s.currentNumber)
	}

	// Set the currentNumber
	s.currentNumber = number

	// Debug
	fmt.Printf("[DEBUG] %+v\n", s)

	return nil
}

func (s *state) GetCurrentNumber() int {
	return s.currentNumber
}

func (s *state) GetPreviousNumbers() []int {
	return s.previousNumbers
}
