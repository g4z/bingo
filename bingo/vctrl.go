package bingo

import (
	"log"

	config "github.com/spf13/viper"
)

//ProcessInput attempts to extract a number from the input
func ProcessInput(buf string) {

	var prefix string
	if config.GetString("voice.lang") == "it-IT" {
		prefix = "numero "
	} else {
		prefix = "number "
	}

	// Check input is a least as long as
	// the prefix and a space char!
	if len(buf) <= len(prefix)+1 {
		return
	}

	// Ignore stupidly long input
	if len(buf) > 25 {
		return
	}

	// Check input against prefix and return if no match
	if string([]rune(buf)[0:len(prefix)-1]) != string([]rune(prefix)[0:len(prefix)-1]) {
		return
	}

	// Remove prefix, we should be left with a number
	buf = string([]rune(buf)[7:])

	// Attempt to convert to an integer
	lang := config.GetString("voice.lang")
	number := numWordToInteger(buf, lang)
	if number == -1 {
		log.Printf("[ERR] Parsing number from string '%s'\n", buf)
		log.Printf("[ERR] Using language: %s\n", lang)
		return
	}

	// Validate the number
	if number < 0 || number > 99 {
		log.Printf("Number out of range: %d (expect 1-99)\n", number)
		return
	}

	if err := State.AddNumber(number); err != nil {
		log.Printf("%s: %d\n", err, number)
		return
	}

	Ack.Play()

	log.Printf("NUMBER(%d)", State.GetCurrentNumber())
}

func numWordToInteger(buf string, lang string) int {
	if val, ok := numWordMap[lang][buf]; ok {
		return val
	}
	return -1
}

var numWordMap = map[string]map[string]int{
	"it-IT": {
		"zero":              0,
		"uno":               1,
		"due":               2,
		"tre":               3,
		"quattro":           4,
		"cinque":            5,
		"sei":               6,
		"sette":             7,
		"otto":              8,
		"nove":              9,
		"dieci":             10,
		"undici":            11,
		"dodici":            12,
		"tredici":           13,
		"quattordici":       14,
		"quindici":          15,
		"sedici":            16,
		"diciassette":       17,
		"diciotto":          18,
		"diciannove":        19,
		"venti":             20,
		"venti uno":         21,
		"ventuno":           21,
		"venti due":         22,
		"venti tre":         23,
		"venti quattro":     24,
		"venti cinque":      25,
		"venti sei":         26,
		"venti sette":       27,
		"venti otto":        28,
		"ventotto":          28,
		"venti nove":        29,
		"trenta":            30,
		"trenta uno":        31,
		"trentuno":          31,
		"trenta due":        32,
		"trenta tre":        33,
		"trenta quattro":    34,
		"trenta cinque":     35,
		"trenta sei":        36,
		"trenta sette":      37,
		"trenta otto":       38,
		"trentotto":         38,
		"trenta nove":       39,
		"quaranta":          40,
		"quaranta uno":      41,
		"quarantuno":        41,
		"quaranta due":      42,
		"quaranta tre":      43,
		"quaranta quattro":  44,
		"quaranta cinque":   45,
		"quaranta sei":      46,
		"quaranta sette":    47,
		"quaranta otto":     48,
		"quarantotto":       48,
		"quaranta nove":     49,
		"cinquanta":         50,
		"cinquanta uno":     51,
		"cinquantuno":       51,
		"cinquanta due":     52,
		"cinquanta tre":     53,
		"cinquanta quattro": 54,
		"cinquanta cinque":  55,
		"cinquanta sei":     56,
		"cinquanta sette":   57,
		"cinquanta otto":    58,
		"cinquantotto":      58,
		"cinquanta nove":    59,
		"sessanta":          60,
		"sessanta uno":      61,
		"sessantuno":        61,
		"sessanta due":      62,
		"sessanta tre":      63,
		"sessanta quattro":  64,
		"sessanta cinque":   65,
		"sessanta sei":      66,
		"sessanta sette":    67,
		"sessanta otto":     68,
		"sessantotto":       68,
		"sessanta nove":     69,
		"settanta":          70,
		"settanta uno":      71,
		"settantuno":        71,
		"settanta due":      72,
		"settanta tre":      73,
		"settanta quattro":  74,
		"settanta cinque":   75,
		"settanta sei":      76,
		"settanta sette":    77,
		"settantotto":       78,
		"settanta nove":     79,
		"ottanta":           80,
		"ottanta uno":       81,
		"ottantuno":         81,
		"ottanta due":       82,
		"ottanta tre":       83,
		"ottanta quattro":   84,
		"ottanta cinque":    85,
		"ottanta sei":       86,
		"ottanta sette":     87,
		"ottanta otto":      88,
		"ottantotto":        88,
		"ottanta nove":      89,
		"novanta":           90,
		"novanta uno":       91,
		"novantuno":         91,
		"novanta due":       92,
		"novanta tre":       93,
		"novanta quattro":   94,
		"novanta cinque":    95,
		"novanta sei":       96,
		"novanta sette":     97,
		"novanta otto":      98,
		"novantotto":        98,
		"novanta nove":      99,
	},
	"en-US": {
		"zero":          0,
		"one":           1,
		"two":           2,
		"three":         3,
		"four":          4,
		"five":          5,
		"six":           6,
		"seven":         7,
		"eight":         8,
		"nine":          9,
		"ten":           10,
		"eleven":        11,
		"twelve":        12,
		"thirteen":      13,
		"fourteen":      14,
		"fifteen":       15,
		"sixteen":       16,
		"seventeen":     17,
		"eighteen":      18,
		"nineteen":      19,
		"twenty":        20,
		"twenty one":    21,
		"twenty two":    22,
		"twenty three":  23,
		"twenty four":   24,
		"twenty five":   25,
		"twenty six":    26,
		"twenty seven":  27,
		"twenty eight":  28,
		"twenty nine":   29,
		"thirty":        30,
		"thirty one":    31,
		"thirty two":    32,
		"thirty three":  33,
		"thirty four":   34,
		"thirty five":   35,
		"thirty six":    36,
		"thirty seven":  37,
		"thirty eight":  38,
		"thirty nine":   39,
		"forty":         40,
		"forty one":     41,
		"forty two":     42,
		"forty three":   43,
		"forty four":    44,
		"forty five":    45,
		"forty six":     46,
		"forty seven":   47,
		"forty eight":   48,
		"forty nine":    49,
		"fifty":         50,
		"fifty one":     51,
		"fifty two":     52,
		"fifty three":   53,
		"fifty four":    54,
		"fifty five":    55,
		"fifty six":     56,
		"fifty seven":   57,
		"fifty eight":   58,
		"fifty nine":    59,
		"sixty":         60,
		"sixty one":     61,
		"sixty two":     62,
		"sixty three":   63,
		"sixty four":    64,
		"sixty five":    65,
		"sixty six":     66,
		"sixty seven":   67,
		"sixty eight":   68,
		"sixty nine":    69,
		"seventy":       70,
		"seventy one":   71,
		"seventy two":   72,
		"seventy three": 73,
		"seventy four":  74,
		"seventy five":  75,
		"seventy six":   76,
		"seventy seven": 77,
		"seventy eight": 78,
		"seventy nine":  79,
		"eighty":        80,
		"eighty one":    81,
		"eighty two":    82,
		"eighty three":  83,
		"eighty four":   84,
		"eighty five":   85,
		"eighty six":    86,
		"eighty seven":  87,
		"eighty eight":  88,
		"eighty nine":   89,
		"ninety":        90,
		"ninety one":    91,
		"ninety two":    92,
		"ninety three":  93,
		"ninety four":   94,
		"ninety five":   95,
		"ninety six":    96,
		"ninety seven":  97,
		"ninety eight":  98,
		"ninety nine":   99,
	},
}
