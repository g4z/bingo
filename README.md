
# bingo

## todo


- fix italian dict
- commands
    - "add number N"
    - "remove number N"
- identify connected user (N users connected)
    - list users / group chat / etc..
- optimize JSON packet
- make funny images a plugin
- simple admin ui + api for config/stats
- add/optimize control words
- select source /  cat /proc/asound/cards

### done

- espeak -> mbrola -> mb-it4
- better speech
- convert numconv.py to golang
- implement total player count
- implement JSON on websocket channel
- dont allow repeat numbers (store history)
- convert numconv.py for italian
- add packr for audio
- control words (eg. "numero tre")
- play wav files with PA
- add packr
- add italian language pack
- document config
- add config mechanism

## nlp

### https://github.com/jdkato/prose

prose is a natural language processing library (English only, at the moment) in pure Go. It supports tokenization, segmentation, part-of-speech tagging, and named-entity extraction.


### github.com/rylans/getlang

getlang provides fast natural language detection in Go.

## speak

### espeak settings

```
# Italian male
espeak-ng -v mb-it3 -s 130 -p20
# Italian female
espeak-ng -v mb-it4 -s 135 -p30
# English male
espeak-ng -v mb-en1 -s 128
```

### mbrola

The Mbrola project is a collection of diphone voices for speech synthesis. They do not include any text-to-phoneme translation, so this must be done by another program. The Mbrola voices are cost-free but are not open source. They are available from the Mbrola website at: http://www.tcts.fpms.ac.be/synthesis/mbrola/mbrcopybin.html

http://espeak.sourceforge.net/mbrola.html

```
sudo apt install mbrola mbrola-en1 mbrola-it3 mbrola-it4 mbrola-us1 mbrola-us2 mbrola-us3
```

## resources

https://github.com/espeak-ng/espeak-ng/blob/master/docs/dictionary.md

- [espeak dictionary tutorial](https://cmusphinx.github.io/wiki/tutorialdict/)

- [espeak dictionary](https://github.com/espeak-ng/espeak-ng/blob/master/docs/dictionary.md)

- [CMUSphinx Dictionary/Model Info #1](https://cmusphinx.github.io/wiki/tutorialdict/)

- [CMUSphinx Dictionary/Model Info #2](https://www.instructables.com/id/Introduction-to-Pocketsphinx-for-Voice-Controled-A/)

- [Original Italian language pack](https://github.com/Uberi/speech_recognition/issues/192)

- [Free speech WAV files for testing](http://www.voiptroubleshooter.com/open_speech/american.html)
