
clean:
	rm -fr ./build
	find -name '*-packr.go' -delete

run-app:
	go run main.go

build-app:
	packr
	go build -o build/bingo main.go
	strip build/bingo

build-base-container:
	docker build -f base.Dockerfile -t bingo/base .

build-dev-container: build-base-container
	docker build -f dev.Dockerfile -t bingo/dev .

build-prod-container: build-app build-base-container
	docker build -f prod.Dockerfile -t bingo/prod .

run-dev-container: build-dev-container
	# export GROUP=`getent group audio | cut -d: -f3`
	docker run --rm -it \
		-p 4000:4000 \
		--device /dev/snd \
		--group-add 29 \
		-e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
		-e DISPLAY=unix${DISPLAY} \
		-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
		-v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
		-v ~/.config/pulse/cookie:/root/.config/pulse/cookie \
		-v ${PWD}:/go/src/gitlab.com/g4z/bingo \
		bingo/dev:latest

run-prod-container: build-prod-container
	docker run --rm -it \
		-p 4000:4000 \
		--device /dev/snd \
		--group-add $(getent group audio | cut -d: -f3) \
		-e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native \
		-e DISPLAY=unix${DISPLAY} \
		-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
		-v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native \
		-v ~/.config/pulse/cookie:/root/.config/pulse/cookie \
		bingo/prod:latest
