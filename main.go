package main

import (
	"sync"

	"./bingo"
)

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	go bingo.ListenHTTP(&wg)
	go bingo.ListenMic(&wg)
	wg.Wait()
}
