FROM bingo/base:latest

# Install Golang
WORKDIR /opt
RUN curl -o go1.11.linux-amd64.tar.gz https://dl.google.com/go/go1.11.linux-amd64.tar.gz
# COPY lib/go1.11.linux-amd64.tar.gz .
RUN tar zxf go1.11.linux-amd64.tar.gz
RUN rm go1.11.linux-amd64.tar.gz
ENV PATH="/opt/go/bin:${PATH}"
RUN mkdir /go
ENV GOPATH /go

# Install deps
WORKDIR /go
RUN go get -v -u github.com/xlab/pocketsphinx-go/sphinx
RUN go get -v -u github.com/xlab/portaudio-go/portaudio
RUN go get -v -u github.com/xlab/closer
RUN go get -v -u github.com/spf13/viper
RUN go get -v -u github.com/gobuffalo/packr/...
RUN go get -v -u github.com/faiface/beep
RUN go get -v -u github.com/hajimehoshi/oto

# Setup project
RUN mkdir -p /go/src/gitlab.com/g4z/bingo
VOLUME [ "/go/src/gitlab.com/g4z/bingo" ]
WORKDIR /go/src/gitlab.com/g4z/bingo
ENTRYPOINT [ "go" ]
CMD [ "run", "main.go" ]
