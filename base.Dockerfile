FROM ubuntu:bionic

RUN echo 'debconf debconf/frontend select Noninteractive' \
    | debconf-set-selections

RUN sed -i 's/^deb-src/#deb-src/g' /etc/apt/sources.list

RUN apt-get update -y && \
    apt-get upgrade -y

RUN apt-get install -y --no-install-recommends \
    automake \
    autoconf \
    bison \
    ca-certificates \
    curl \
    espeak-ng \
    gcc \
    git \
    libportaudio2 \
    libpulse-dev \
    libtool \
    locales \
    mbrola-en1 \
    mbrola-it3 \
    mbrola-it4 \
    mbrola-us1 \
    mbrola-us2 \
    mbrola-us3 \
    pkg-config \
    portaudio19-dev \
    python-dev \
    pulseaudio \
    swig \
    unzip

RUN apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists /usr/share/man /usr/share/doc

# Configure PulseAudio
ENV PULSE_SERVER /run/pulse/native

# Set locale to UTF8
RUN locale-gen --no-purge en_GB.UTF-8
RUN update-locale LANG=en_GB.UTF-8
RUN dpkg-reconfigure --frontend=noninteractive locales
ENV LANGUAGE en_GB.UTF-8
ENV LANG en_GB.UTF-8
ENV LC_ALL en_GB.UTF-8

# Install Sphinxbase
WORKDIR /opt
RUN git clone -v https://github.com/cmusphinx/sphinxbase
RUN git clone -v https://github.com/cmusphinx/pocketsphinx
# COPY lib/sphinxbase sphinxbase
# COPY lib/pocketsphinx pocketsphinx
WORKDIR /opt/sphinxbase
RUN ./autogen.sh
RUN make
RUN make install
WORKDIR /opt/pocketsphinx
RUN ./autogen.sh
RUN make
RUN make install
WORKDIR /opt
RUN rm -fr sphinxbase pocketsphinx
RUN ldconfig

# Install Italian Language Pack
WORKDIR /opt
RUN git clone -v https://gitlab.com/g4z/pocketsphinx-lang-italian
# COPY lib/pocketsphinx-lang-italian pocketsphinx-lang-italian
WORKDIR /opt/pocketsphinx-lang-italian
RUN unzip it-IT.zip
RUN mv pocketsphinx-data/it-IT /usr/local/share/pocketsphinx/model/
# Remove these we dont use them
RUN rm -f /usr/local/share/pocketsphinx/model/en-us/cmudict-en-us.dict
RUN rm -f /usr/local/share/pocketsphinx/model/it-IT/pronounciation-dictionary.dict
RUN rm -f /usr/local/share/pocketsphinx/model/en-us/en-us-phone.lm.bin
# Add custom dictionaries
COPY dict/en.custom.dict /usr/local/share/pocketsphinx/model/en-us/en.custom.dict
COPY dict/it.custom.dict /usr/local/share/pocketsphinx/model/it-IT/it.custom.dict
WORKDIR /opt
RUN rm -fr pocketsphinx-lang-italian

# Default to /
WORKDIR /
