FROM bingo/base:latest

# TODO: Strip out a lot of stuff here

WORKDIR /srv
COPY numconv.py .
COPY bingo .
RUN chmod 755 /srv/bingo
ENTRYPOINT [ "/srv/bingo" ]
